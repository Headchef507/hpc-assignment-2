#include <stdio.h>
#include <math.h>
#include <mpi.h>
#include <stdlib.h>

#pragma region SIMULATION CONFIG - START
// Here you can change, the behaviour of the program

// This is the amount of processes which this system will get. 
// This will generate a 2d array of size (sqrt(SIZE) x sqrt(SIZE)) 
// !!NOTE!!: you only need to change this variable to change everything 
//           about the cartesian structure and the MPI communications
#define SIZE 16

// This indicates how many cycles of fishing simulation should we perform
// the total amount of MPI calls will result in = 2 + 1xBCAST + (AMOUNTOFCYCLES * sqrt(SIZE))
#define AMOUNTOFCYCLES 16

// You can define how many boats and groups of fish you want
// NOTE: you need to have the same amount of elements in AMOUNTOFFISHBOATCANHOLD and AMOUNTOFFISHBOATCANFISH
#define AMOUNTOFBOATGROUPS 2
#define AMOUNTOFFISHGROUPS 2

// NOTE: first index in the array corresponds to 1'st group in AMOUNTOFBOATGROUPS and AMOUNTOFFISHGROUPS
const int AMOUNTOFFISHBOATCANHOLD[] = { 50, 33 };
const int AMOUNTOFFISHBOATCANFISH[] = { 24, 12 };

#pragma endregion SIMULATION CONFIG - END

#pragma region OUTPUT CONFIG - START 
// here you can define what outputs do you want to see

// Debugg will print out a lot more information, some of it will be redudant.
//#define DEBUGG 1

// if this is set then it will time and output all the pefromance metrics
#define TIMESIMULATION 1

// if this is set then the system will generate a special type of output
// which can be read by our custom visualization tool
#define VISUALIZEROUTPUT 1

#pragma endregion OUTPUT CONFIG - END

// !!!!! NOT ALLOWED TO CHANGE THIS !!!!!!!
// change results in potential system crash
// These are indexes for arrays, each proccess has an array of four
// where each entry is it's neigbour process (UP = above the current process, Down = below the current process, etc..)
#define UP 0
#define DOWN 1
#define LEFT 2
#define RIGHT 3

#define LAND 0
#define SEA 1

// this is our custom object which our processes will communicate with.
// NOTE: Originnaly we wanted to create a Boat and Fish structures which could reside in CustomComsObject as arrays
//       After reading about it, this would be quite complex to implement for such a short gain.
//       So instead we use array's where the index indicates what group it is and value to indicate both if it's present and it's amount.
struct CustomComsObject 
{
  // -1 indicates that the fish is not inside
  // >= 0 indicates the fish is inside and the amount of fish on that boat is the value
  int fish[AMOUNTOFFISHGROUPS];
  // -1 indicates that the boat is not inside
  // >= 0 indicates the boat is inside and the amount of fish on that boat is the value 
  int boats[AMOUNTOFBOATGROUPS];
  int value; // used for random values and calculations.
};

/*
  Usage: Build_CustomComsObject(customObject, newSturct)
    For: customObject is CustomComsObject struct
  After: creates and stores the pointer to a MPI_Datatype in newSturct which is equivelant to the 'CustomComsObject' structure
*/
void Build_CustomComsObject(struct CustomComsObject customObject, MPI_Datatype* newSturct)
{  
  // our 'CustomComsObject' has 3 variables (for now).
  int block_len[3];
  MPI_Aint displacement[3];
  MPI_Datatype types[3];

  block_len[0]= AMOUNTOFFISHGROUPS;
  block_len[1]= AMOUNTOFBOATGROUPS;
  block_len[2]= 1;

  // define the types of variables in the object
  types[0] = MPI_INT;
  types[1] = MPI_INT;
  types[2] = MPI_INT;

  // get the addresses for each value and store them in the displacements 
  #pragma GCC diagnostic ignored "-Wdeprecated-declarations"
  MPI_Address(&customObject.fish, &displacement[0]);

  #pragma GCC diagnostic ignored "-Wdeprecated-declarations"
  MPI_Address(&customObject.boats, &displacement[1]);

  #pragma GCC diagnostic ignored "-Wdeprecated-declarations"
  MPI_Address(&customObject.value, &displacement[2]);

  // next we need to calculate the displacements of each variable this is done by
  // taking the address space from the current variable and subtracting it from displacement[0]
  displacement[2] = displacement[2] - displacement[0];

  displacement[1] = displacement[1] - displacement[0];

  // the first variable always has the displacement 0
  displacement[0] = 0;

  // Commit the structure.
  #pragma GCC diagnostic ignored "-Wdeprecated-declarations"
  MPI_Type_struct(3, block_len, displacement, types, newSturct);
  MPI_Type_commit(newSturct);
}

/*
  Usage: MakeEmptyComs(comRef)
    For: comRef is a non-null reference to a CustomComsObject structure
  After: sets all the values inside comRef to their defaults.
 */
void MakeEmptyComs(struct CustomComsObject* comRef)
{
  comRef->value = -1;
  for(int i = 0; i < AMOUNTOFFISHGROUPS; i++)
  {
    comRef->fish[i] = -1;
  }
  
  // This can be parrelized using OpenMP
  for(int i = 0; i < AMOUNTOFBOATGROUPS; i++)
  {
    comRef->boats[i] = -1;
  }
}

#pragma region Enviromental setup helpers - START 
// The idea of for this portion of the enviroment is to minimize the
// amount of changes which we need to do in the main function to recude the amount of errors.

/*
  This is where we 'shape' our enviroment this function basically is responsible 
  for assigning which proccessor in the 2D array should be land and which should be water.
 */
int AmILand(int xLoc, int yLoc, int dimSize)
{ 
  // Our enviroment will always create a 1 sector mass of land in the middle of the world for all amounts of proccesors.
  int middleOfDIm = dimSize/2;
  
  if(xLoc == middleOfDIm && yLoc == middleOfDIm)
  {
    return LAND;
  }
  else
  {
    return SEA;
  }
}

/*
  this function decides which process should inherit the role of the harbour.
  If we want to add multiple lands and have land which are not harbours we can add this code here.
*/
int AmIHarbour(int xLoc, int yLoc, int type)
{
  if(type == LAND)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/*
  This function decides what processes should contain fish, currently 
  it will place fishes in the topleft and right corners.
 */
int ShouldIContainFish(int xLoc, int yLoc, int dimSize, int type, int termFlag )
{
  if(termFlag == 1)
  {
    return -1;
  }
  else if(((xLoc == 0) && (yLoc == 0 || yLoc == dimSize-1)) && type != LAND)
  {
    return  (rand() % 100) + 50;
  }
  else
  {
    return -1;
  }
}

#pragma endregion Enviromental setup helpers - END

#pragma region Helper functions for finishing simulation - START  

// returns 1 if the comRef structure contains boats, otherwise -1
int IContainBoat(struct CustomComsObject* comRef)
{
  for(int i = 0; i < AMOUNTOFBOATGROUPS; i++)
  {
    if(comRef->boats[i] != -1)
    {
      return i;
    }
  }

  return -1;
}

// returns 1 if the comRef structure contains fish, otherwise -1
int IContainFish(struct CustomComsObject* comRef)
{
  for(int i = 0; i < AMOUNTOFFISHGROUPS; i++)
  {
    if(comRef->fish[i] != -1)
    {
      return i;
    }
  }

  return -1;
}

// method to determine in which direction should a boat go to reach the harbour
int boatFullLoad(int harbourLocation[], int coords[])
{
	if(coords[1] < harbourLocation[1])
	{
		return RIGHT;
	}
	else if(coords[1] > harbourLocation[1])
	{
		return LEFT;
	}
	else if(coords[0] < harbourLocation[0])
	{
		return DOWN;
	}
	else if(coords[0] > harbourLocation[0])
	{
		return UP;
	}

	return -1;
}

#pragma endregion Helper functions for finishing simulation - END

int main (int argc, char** argv) 
{
  /* We split this program into three parts.
    1. the setup of the MPI enviroment
      - create a cartesian grid
      - initialize all the values (what rank is the process where in the grid is it) 
      - create a custom MPI structure
      - etc..

    2. Word setup
      - Assigning LAND/SEA roles to processes
      - Assigning role of Harbour
      - placing fishes and boats in their initial positions 
      - etc..

    3. Simulation
      - Simulate fish and boat movement
      - boats fishing
      - harbour getting fish from boats   
  */    
  #pragma region MPI setup - START

  int numtasks, rank, source, dest, myType, amIHarbour,harbourAmountOfFish, dimSize;
  int harbourLocation[2]; // this will contain the location of the harbour
  int rootRank = 0; // All the processes can send information to the root rank and that rank will BroadCast that information

  dimSize = ((int)sqrt(SIZE));
  // MPI_PROC_NULL is a constant that represents a dummy MPI process rank. When passing it to a send or receive operation for instance, it is guaranteed to succeed and return as soon as possible.
  struct CustomComsObject inbuf[4]={MPI_PROC_NULL,MPI_PROC_NULL, MPI_PROC_NULL,  MPI_PROC_NULL};
  // nbrs will contain all the neighbour ranks of the current processes, i.e rank of the neighbours above below, left, and right.
  int nbrs[4];
  // nbrsType will contain what types of cell it is i.e. Water or Land
  int nbrsType[4];
  
  // dims   : contains the size of the cartesian grid which is a 2D array
  // periods: is config which tells what should happens for communications on the end nodes e.g. should final element in array send its information to the first element or not.
  //          1,1 basically says that they should communicate
  // reorder: indicates that ranking may be reordered (true) or not (false) (logical)
  int dims[2] = {dimSize,dimSize}, periods[2] = {1,1}, reorder=1;
  
  // this will contain the current proccesses location in the 2D array.
  int coords[2];

  // will contain the new communicator
  MPI_Comm cartcomm;
  MPI_Request reqs[8];
  MPI_Status stats[8];
  
  // starting with MPI program
  MPI_Init(&argc, &argv);

  MPI_Comm_size(MPI_COMM_WORLD, &numtasks);
  // Create a new 2d communicator and store it in cartcomm
  MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods,reorder, &cartcomm);
  // obtain the rank of the current proccess inside the cartcomm (AND NOT THE MPI_COMM_WORLD!!!)
  MPI_Comm_rank(cartcomm, &rank);
  // This will fetch the current procceses location in the 2D array and store it in coords.
  MPI_Cart_coords(cartcomm, rank, 2, coords);
  // MPI_Cart_shift returns the source and destination proccess ranks for a given shift direction
  // here we perform two shifts and obtain the current proccesses neighbours which will be stored in nbrs.
  MPI_Cart_shift(cartcomm, 0, 1, &nbrs[UP], &nbrs[DOWN] );
  MPI_Cart_shift(cartcomm, 1, 1, &nbrs[LEFT], &nbrs[RIGHT]);

  #ifdef DEBUGG
    printf("rank= %d located in coords= %d %d has the following neighbours(up,down,left,right)= %d %d %d %d \n", rank, coords[0], coords[1], nbrs[UP], nbrs[DOWN], nbrs[LEFT], nbrs[RIGHT]);
  #endif

  // Create and commit the new DataType which the proccesses will use.
  MPI_Datatype comMessageStruct;
  struct CustomComsObject custmsgobject;
  Build_CustomComsObject(custmsgobject, &comMessageStruct);

  #pragma endregion MPI setup - END
  
  #pragma region Enviroment setup - START

  harbourAmountOfFish = 0;
  myType = AmILand(coords[0], coords[1], dimSize);
  amIHarbour = AmIHarbour(coords[0], coords[1], myType);

  // The harbour will store its location in the harbourLocation buffer which will
  // be broadcases to all the processes, so they know where it is.
  if(amIHarbour == 1)
  {
    harbourLocation[0] = coords[0];
    harbourLocation[1] = coords[1];

    // harbour is going to send this information to the root rank so the root rank can broadcast it to all the others
    MPI_Send(&harbourLocation,2,MPI_INT,rootRank,0,cartcomm);
  }

  // Assign fish to process
  if(((coords[0] == 0) && (coords[1] == 0)) && myType != LAND)
  {
    custmsgobject.fish[0] = (rand() % 100) + 50;
  }
  else 
  {
    custmsgobject.fish[0] = -1;
  }
  if(((coords[0] == 0) && (coords[1] == dimSize-1)) && myType != LAND)
  {
    custmsgobject.fish[1] = (rand() % 100) + 50;
  }
  else {
    custmsgobject.fish[1] = -1;
  }

  #ifdef VISUALIZEROUTPUT
    // This can be parrelized using OpenMP
    for(int i = 0; i < AMOUNTOFFISHGROUPS; i++)
    {
      if(custmsgobject.fish[i] != -1)
      {
        // Process located on x,y contains fish group 'i' with  > 0 amount of fish
        printf("VIZUALIZATOR FISH %d %d %d %d \n",coords[0], coords[1], i, custmsgobject.fish[i]);
      }
    }
  #endif 
  
  // This can be parrelized using OpenMP
  for(int i = 0; i < AMOUNTOFBOATGROUPS; i++)
  {
    if(amIHarbour == 1)
    {
      custmsgobject.boats[i] = 0;
      
      #ifdef VISUALIZEROUTPUT
        // Process located on x,y contains Boat 'i' with  > 0 amount of fish
        printf("VIZUALIZATOR BOAT %d %d %d %d \n",coords[0], coords[1], i, custmsgobject.boats[i]);
      #endif 
    }
    else
    {
      custmsgobject.boats[i] = -1;
    }
  }

  // we have to notify our neigbours what type (land/sea) we are
  // so we see the generic value place for this.
  custmsgobject.value = myType;  

  #ifdef DEBUGG
    if(myType == 1)
    {
      printf("rank= %d located in coords= %d %d is WATER \n", rank, coords[0], coords[1]);
    }
    else
    {
      printf("rank= %d located in coords= %d %d is LAND \n", rank, coords[0], coords[1]);
    }
    
    if(amIHarbour == 1)
    {
      printf("rank= %d located in coords= %d %d is HARBOUR \n", rank, coords[0], coords[1]);
    }
  #endif

  #ifdef VISUALIZEROUTPUT
    printf("VIZUALIZATOR LANDWATER %d %d %d \n",coords[0], coords[1], myType);
  #endif 

  // if the current process is the rootrank then, it will recive
  // information from some other process which claims to be the harbour
  if(rootRank == rank)
  {
    // one thing to keep in mind, we do not know who will send us this information
    // we just know that we should recive one instance of this information, so we use MPI_ANY_SOURCE
    MPI_Recv(&harbourLocation, 2, MPI_INT, MPI_ANY_SOURCE, MPI_ANY_TAG, cartcomm, MPI_STATUS_IGNORE);

    printf("Process at cords= %d %d is the harbour \n", harbourLocation[0], harbourLocation[1]);
  }

  // now the RootRank will Broadcast the location of the harbour to all the processes
  MPI_Bcast(harbourLocation,2,MPI_INT,rootRank,cartcomm);

  // Instead of sending the land bit over and over again to each process
  // We let all the proccesses communciate their type with each other and let each proccess store it
  // that way we do not need to send the landmass information each time
  for (int i=0; i < 4;i++) 
  {
    // define the current send destination
    dest=nbrs[i];
    // define the current recive
    source=nbrs[i];

    // perform non-blocking communication
    // Note the 5th portion in MPI_Isend & MPI_Irecv is the tag, I do not use it because 
    // then i would require to check the status of each message as well (i am to lazy for that shit.)

    // send our neighbour the customobject which this time contains the type of section 'land' or 'water' in the 'value' field 
    MPI_Isend(&custmsgobject, 1, comMessageStruct, dest, 1, MPI_COMM_WORLD, &reqs[i]);
    // recive information which will be stored in inbuf from the current source. 
    MPI_Irecv(&inbuf[i], 1, comMessageStruct, source, 1, MPI_COMM_WORLD, &reqs[i+4]); //  kind of offset (not sure why)
  }
  
  // wait for non-blocking communication to be completed for output  
  MPI_Waitall(8, reqs, stats);

  // now the current proccess stores the type of cells which his neighbours are
  nbrsType[UP] = inbuf[UP].value;
  nbrsType[DOWN] = inbuf[DOWN].value;
  nbrsType[LEFT] = inbuf[LEFT].value;
  nbrsType[RIGHT] = inbuf[RIGHT].value;

  #ifdef DEBUGG
    printf("rank= %d, located in coords= %d %d has received information from its neighbours\nUp neighbour is considered= %d\nDown neighbour is considered= %d\nLeft neighbour is considered= %d\nRight neighbour is considered= %d\n\n\n",rank, coords[0], coords[1],nbrsType[UP],nbrsType[DOWN],nbrsType[LEFT],nbrsType[RIGHT]);
  #endif

  #pragma endregion Enviroment setup - END
 
  #pragma region  Fishing simulation - START

  #ifdef TIMESIMULATION
    double time1, time2;
    time1 = MPI_Wtime();
  #endif

  for(int i = 0; i < AMOUNTOFCYCLES; i++)
  { 
    // early termination flag to see if the current process needs to perform boating logic.
    int IGotBoatsThisRound = 0;
    // flag to see if the boats needs to head to harbour instead of going in random direction
    int headBackToHarbour = -1;
    // On each itteration, we will store the information from all of our neighbours into stagingObject
    // and perform all the calculations from one object.
    struct CustomComsObject stagingObject;
    MakeEmptyComs(&stagingObject);
    
    // during the first iteration, inbuf contains a bunch of null values
    if(i != 0)
    {
      // Go over each recived value and add it to the stagingObject
      // Can pallarize this with openMP
      for (int j=0; j < 4; j++) 
      {
        // Can pallarize this with openMP
        for(int f = 0; f < AMOUNTOFBOATGROUPS; f++)
        {
          if(inbuf[j].boats[f] >= 0)
          {
            stagingObject.boats[f] = inbuf[j].boats[f];
            IGotBoatsThisRound = 1;

            #ifdef VISUALIZEROUTPUT
              printf("VIZUALIZATOR BOATLOC %d %d %d %d %d \n",coords[0], coords[1], i, f ,inbuf[j].boats[f]);
            #endif 

            printf("A boat has sailed from rank %d over to rank %d located in coords= %d %d \n",nbrs[j], rank, coords[0], coords[1]);
          }    
        }
        // Can pallarize this with openMP
        for(int f1 = 0; f1 < AMOUNTOFFISHGROUPS; f1++)
        {
          if(inbuf[j].fish[f1] >= 0)
          {
            stagingObject.fish[f1] = inbuf[j].fish[f1];
            #ifdef VISUALIZEROUTPUT
              printf("VIZUALIZATOR FISHLOC %d %d %d %d %d \n",coords[0], coords[1], i, f1 ,inbuf[j].fish[f1]);
            #endif 

            printf("Fish swam from rank %d over to rank %d located in coords= %d %d \n",nbrs[j], rank, coords[0], coords[1]);
          }    
        }
      }
    }
    else
    {
      stagingObject = custmsgobject;
    }

    // if the current process contains boats then perform boating logic
    if (IGotBoatsThisRound == 1)
    {
      // perform fishing logic
      for (int b = 0; b < AMOUNTOFBOATGROUPS; b++)
      {
        if ((IContainFish(&stagingObject) != -1) && (stagingObject.boats[b] != -1) && (stagingObject.boats[b] < AMOUNTOFFISHBOATCANHOLD[b]))
        {
          int howManyFishGroupsDoIHave = 0;
          for (int i = 0; i < AMOUNTOFFISHGROUPS; i++)
          {
            if (stagingObject.fish[i] != -1)
            {
              howManyFishGroupsDoIHave += 1;
            }
          }

          int freeSpace = AMOUNTOFFISHBOATCANHOLD[b] - stagingObject.boats[b];
          int howMuchToFishNow = 0;

          if (freeSpace > AMOUNTOFFISHBOATCANFISH[b])
          {
            howMuchToFishNow = AMOUNTOFFISHBOATCANFISH[b];
          }
          else
          {
            howMuchToFishNow = freeSpace;
          }
          howMuchToFishNow = howMuchToFishNow / howManyFishGroupsDoIHave;

          int howMuchWasActuallyFished = 0;

          for (int f = 0; f < AMOUNTOFFISHGROUPS; f++)
          {
            if (stagingObject.fish[f] != -1)
            {

              //If there is enough fish then the the boat can fish in this cycle
              // then the boat fishes as much as it can
              if (howMuchToFishNow < stagingObject.fish[f])
              {
                stagingObject.boats[b] = stagingObject.boats[b] + howMuchToFishNow;
                stagingObject.fish[f] = stagingObject.fish[f] - howMuchToFishNow;
                howMuchWasActuallyFished = howMuchWasActuallyFished + howMuchToFishNow;
              }
              //...If not then we fish this fish group to extinction
              else
              {
                stagingObject.boats[b] = stagingObject.boats[b] + stagingObject.fish[f];
                stagingObject.fish[f] = -1;
                howMuchWasActuallyFished = howMuchWasActuallyFished + stagingObject.fish[f];
              }
            }
          }
          
          #ifdef TIMESIMULATION
            if (stagingObject.boats[b] >= AMOUNTOFFISHBOATCANHOLD[b])
            {
              time2 = MPI_Wtime();
              printf("CAPTAINS LOG: BOAT #%d has a full cargo in iteration:%d. Elapsed time of program segment is %f \n", b, i, time2 - time1);
            }
          #endif

          printf("CAPTAINS LOG: BOAT #%d Fished %d fishes at coords %d %d \n", b, howMuchWasActuallyFished, coords[0], coords[1]);
        }
        else if ((stagingObject.boats[b] != -1) && (stagingObject.boats[b] >= AMOUNTOFFISHBOATCANHOLD[b]))
        {
          headBackToHarbour = boatFullLoad(harbourLocation, coords);
          printf("CAPTAINS LOG: BOAT #%d located at coords %d %d is heading back to the harbour \n", b, coords[0], coords[1]);
        }
      }

      // if the current process contains boats and it is the harbour, 
      // harbour logic needs to be performed as well
      if(amIHarbour == 1)
      {
        #ifdef DEBUGG
          printf("rank= %d located in coords= %d %d is the harbour and boats have docked to offload fish  \n", rank, coords[0], coords[1]);
        #endif

        // Check all the boats and extract the fishes from them.
        // Can pallarize this with openMP
        for(int f = 0; f < AMOUNTOFBOATGROUPS; f++)
        {
          if(stagingObject.boats[f] >= 0)
          {
            harbourAmountOfFish += stagingObject.boats[f];
            stagingObject.boats[f] = 0;
          }    
        }
        printf("rank= %d located in coords= %d %d which is the harbour has collected fish from the boats in the harbour and now has %d amount of fish \n", rank, coords[0], coords[1], harbourAmountOfFish);
      }
    }
    
    // during each sending operation there is 25% chance to move in that direction
    // and for each land as your neighbour increase these ods by 25%
    // this way at the end there is always 100% chance to move a boat or fish group
    // in a direction that is not a land 
    int currentOdsOfMovingShip = 75;
    int currentOdsOfMovingFish = 75;
    for (int j=0; j < 4;j++) 
    {
      if(nbrsType[j] == LAND)
      {
        currentOdsOfMovingShip -= 25;
        currentOdsOfMovingFish -= 25;
      }
    }
    
    for (int j=0; j < 4;j++) 
    {
      dest=nbrs[j];
      source=nbrs[j];
      MakeEmptyComs(&custmsgobject);

      int boatLocation = IContainBoat(&stagingObject);
      if( nbrsType[j] == SEA && boatLocation != -1 && headBackToHarbour == -1 && (rand() % 100) >= currentOdsOfMovingShip || 
          headBackToHarbour != -1 && boatLocation != -1 && nbrs[headBackToHarbour] == dest)
      {
        custmsgobject.boats[boatLocation] = stagingObject.boats[boatLocation];
        stagingObject.boats[boatLocation] = -1;

        #ifdef DEBUGG
          printf("rank= %d located in coords= %d %d sending boat to rank %d  \n", rank, coords[0], coords[1], nbrs[j]);
        #endif
      }

      int fishLocation = IContainFish(&stagingObject);
      if(nbrsType[j] == SEA && fishLocation != -1 && (rand() % 100) >= currentOdsOfMovingFish)
      {
        custmsgobject.fish[fishLocation] = stagingObject.fish[fishLocation];
        stagingObject.fish[fishLocation] = -1;

        #ifdef DEBUGG
          printf("rank= %d located in coords= %d %d sending fish to %d  \n", rank, coords[0], coords[1], nbrs[j]);
        #endif
      }

      MPI_Isend(&custmsgobject, 1, comMessageStruct, dest, 1, MPI_COMM_WORLD, &reqs[j]);
      MPI_Irecv(&inbuf[j], 1, comMessageStruct, source, 1, MPI_COMM_WORLD, &reqs[j+4]);

      currentOdsOfMovingShip -= 25;
      currentOdsOfMovingFish -= 25;
    }

    // wait for non-blocking communication to be completed for output  
    MPI_Waitall(8, reqs, stats);
  }
 
  #ifdef TIMESIMULATION
    time2 = MPI_Wtime();
    printf("rank= %d located in coords= %d %d took %f to finish running the simulation \n", rank, coords[0], coords[1], (time2 - time1));        
  #endif

  #pragma endregion Fishing simulation - END
  
  // need to remember to clean the custom struct
  MPI_Type_free(&comMessageStruct);
  // need to remeber to clear the communicator.
  MPI_Comm_free(&cartcomm);
  
  MPI_Finalize();

  return 0;
}
